### ConcentrationGame Demos
#### Low Level Play
![Low Level Play](https://media.giphy.com/media/5pWN1HxVpBo7lLfVhA/giphy.gif)

#### Intermediate Level Play
![Intermediate Level Play](https://media.giphy.com/media/1jaJPA89l0g540Fj0D/giphy.gif)

#### Close & Restart Game
![Close & Restart Game](https://media.giphy.com/media/9AIXMtu0b7WQNNFf2h/giphy.gif)

#### Show Game History
![Show Game History](https://media.giphy.com/media/lvOoHpyz1GcopWQssj/giphy.gif)